/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}genera
 */
export const preloadHandlebarsTemplates = async function () {

  // Define template paths to load
  const templatePaths = ['__ALL_TEMPLATES__'];

  const paths = {}
  for (const path of templatePaths) {
    paths[path] = path
    paths[`r3a.${path.split("/").pop().replace(".hbs", "")}`] = path;
  }
  // Load the template parts
  return loadTemplates(paths);
};

export function registerHandlebarsHelpers() {
  Handlebars.registerHelper("getProperty", foundry.utils.getProperty)
  Handlebars.registerHelper("itemContext", (context, options) => {
    if ( foundry.utils.getType(context) === "function" ) context = context.call(this);

    const ctx = options.data.root.itemContext?.[context.id];
    if ( !ctx ) {
      const inverse = options.inverse(this);
      if ( inverse ) return options.inverse(this);
    }

    return options.fn(context, { data: options.data, blockParams: [ctx] });
  })
}
