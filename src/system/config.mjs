const R3A = {}

R3A.attributes = {
    strength: "R3A.CARACTERISTICAS.FUERZA",
    constitution: "R3A.CARACTERISTICAS.CONSTITUCION",
    willpower: "R3A.CARACTERISTICAS.VOLUNTAD",
    charisma: "R3A.CARACTERISTICAS.CARISMA",
    dexterity: "R3A.CARACTERISTICAS.DESTREZA",
    perception: "R3A.CARACTERISTICAS.PERCEPCION",
    essence: "R3A.CARACTERISTICAS.ESENCIA",
    intelligence: "R3A.CARACTERISTICAS.INTELIGENCIA",
}
R3A.hp = {
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 6,
    6: 8,
    7: 10,
    8: 12,
    9: 14,
    10: 16
}
R3A.da = {
    1: 11,
    2: 11,
    3: 11,
    4: 12,
    5: 14,
    6: 15,
    7: 17,
    8: 18,
    9: 20,
    10: 23,
}
R3A.bd = {
    1: '-1d4',
    2: '-1',
    3: '-',
    4: '-',
    5: '-',
    6: '-',
    7: '-',
    8: '+1',
    9: '+1d4',
    10: '+1d6',
}
R3A.difficulty = {
    11: 'R3A.DIFICULTAD.MUY_FACIL',
    14: 'R3A.DIFICULTAD.FACIL',
    17: 'R3A.DIFICULTAD.NORMAL',
    21: 'R3A.DIFICULTAD.DIFICIL',
    25: 'R3A.DIFICULTAD.MUY_DIFICIL',
    30: 'R3A.DIFICULTAD.IMPOSIBLE',
    35: 'R3A.DIFICULTAD.MILAGROSO',
}
R3A.spell_difficulty = {
    ...R3A.difficulty,
    0: 'R3A.DIFICULTAD.VARIABLE'
}
R3A.actor_stress_difficulty = {
    0: 'R3A.DIFICULTAD.NINGUNA',
    ...R3A.difficulty
}
R3A.skills = {
    cac: {label: "R3A.HABILIDADES.CUERPO_A_CUERPO", attribute: 'strength'},
    pel: {label: "R3A.HABILIDADES.PELEA", attribute: 'strength'},
    atl: {label: "R3A.HABILIDADES.ATLETISMO", attribute: 'constitution'},
    con: {label: "R3A.HABILIDADES.CONCENTRACION", attribute: 'willpower'},
    fri: {label: "R3A.HABILIDADES.FRIALDAD", attribute: 'willpower'},
    sup: {label: "R3A.HABILIDADES.SUPERVIVENCIA", attribute: 'willpower', disadvantage_on_null: true},
    cc: {label: "R3A.HABILIDADES.CONOCIMIENTO_CALLE", attribute: 'charisma', disadvantage_on_null: true},
    dis: {label: "R3A.HABILIDADES.DISFRAZARSE", attribute: 'charisma'},
    int: {label: "R3A.HABILIDADES.INTIMIDAR", attribute: 'charisma'},
    per: {label: "R3A.HABILIDADES.PERSUASION", attribute: 'charisma'},
    sed: {label: "R3A.HABILIDADES.SEDUCCION", attribute: 'charisma'},
    ad: {label: "R3A.HABILIDADES.ARMAS_DISTANCIA", attribute: 'dexterity', disadvantage_on_null: true},
    af: {label: "R3A.HABILIDADES.ARMAS_FUEGO", attribute: 'dexterity', disadvantage_on_null: true},
    cnd: {label: "R3A.HABILIDADES.CONDUCIR", attribute: 'dexterity', disadvantage_on_null: true},
    esq: {label: "R3A.HABILIDADES.ESQUIVAR", attribute: 'dexterity'},
    ex: {label: "R3A.HABILIDADES.EXPLOSIVOS", attribute: 'dexterity', can_roll_on_null: false},
    lat: {label: "R3A.HABILIDADES.LATROCINIO", attribute: 'dexterity', disadvantage_on_null: true},
    mon: {label: "R3A.HABILIDADES.MONTAR", attribute: 'dexterity', disadvantage_on_null: true},
    nav: {label: "R3A.HABILIDADES.NAVEGAR", attribute: 'dexterity', can_roll_on_null: false},
    pil: {label: "R3A.HABILIDADES.PILOTAR", attribute: 'dexterity', can_roll_on_null: false},
    sig: {label: "R3A.HABILIDADES.SIGILO", attribute: 'dexterity', can_roll_on_null: false},
    ti: {label: "R3A.HABILIDADES.TOCAR_INSTRUMENTO", attribute: 'dexterity', has_description: true},
    ab: {label: "R3A.HABILIDADES.ARCHIVOS_BIBLIOTECAS", attribute: 'perception'},
    alt: {label: "R3A.HABILIDADES.ALERTA", attribute: 'perception'},
    bus: {label: "R3A.HABILIDADES.BUSCAR", attribute: 'perception'},
    intu: {label: "R3A.HABILIDADES.INTUICION", attribute: 'perception'},
    ori: {label: "R3A.HABILIDADES.ORIENTACION", attribute: 'perception'},
    ant: {label: "R3A.HABILIDADES.ANTROPOLOGIA", attribute: 'intelligence', can_roll_on_null: false},
    arq: {label: "R3A.HABILIDADES.ARQUEOLOGIA", attribute: 'intelligence', can_roll_on_null: false},
    art: {label: "R3A.HABILIDADES.ARTE", attribute: 'intelligence', disadvantage_on_null: false},
    astg: {label: "R3A.HABILIDADES.ASTROLOGIA", attribute: 'intelligence', disadvantage_on_null: true},
    astm: {label: "R3A.HABILIDADES.ASTRONOMIA", attribute: 'intelligence', disadvantage_on_null: true},
    bio: {label: "R3A.HABILIDADES.BIOLOGIA", attribute: 'intelligence', can_roll_on_null: false},
    co: {label: "R3A.HABILIDADES.CIENCIAS_OCULTAS", attribute: 'intelligence', can_roll_on_null: false},
    crip: {label: "R3A.HABILIDADES.CRIPTOGRAFIA", attribute: 'intelligence', can_roll_on_null: false},
    der: {label: "R3A.HABILIDADES.DERECHO", attribute: 'intelligence', can_roll_on_null: false},
    eco: {label: "R3A.HABILIDADES.ECONOMIA", attribute: 'intelligence', disadvantage_on_null: true},
    elec: {label: "R3A.HABILIDADES.ELECTRONICA", attribute: 'intelligence', disadvantage_on_null: true},
    fis: {label: "R3A.HABILIDADES.FISICA", attribute: 'intelligence', can_roll_on_null: false},
    geog: {label: "R3A.HABILIDADES.GEOGRAFIA", attribute: 'intelligence', disadvantage_on_null: true},
    geol: {label: "R3A.HABILIDADES.GEOLOGIA", attribute: 'intelligence', disadvantage_on_null: true},
    his: {label: "R3A.HABILIDADES.HISTORIA", attribute: 'intelligence', disadvantage_on_null: true},
    in: {label: "R3A.HABILIDADES.IDIOMA_NATIVO", attribute: 'intelligence', has_description: true},
    inf: {label: "R3A.HABILIDADES.INFORMATICA", attribute: 'intelligence', disadvantage_on_null: true},
    mec: {label: "R3A.HABILIDADES.MECANICA", attribute: 'intelligence', disadvantage_on_null: true},
    med: {label: "R3A.HABILIDADES.MEDICINA", attribute: 'intelligence', can_roll_on_null: false},
    para: {label: "R3A.HABILIDADES.PARAPSICOLOGIA", attribute: 'intelligence', can_roll_on_null: false},
    pa: {label: "R3A.HABILIDADES.PRIMEROS_AUXILIOS", attribute: 'intelligence', disadvantage_on_null: true},
    psi: {label: "R3A.HABILIDADES.PSICOLOGIA", attribute: 'intelligence', disadvantage_on_null: true},
    qui: {label: "R3A.HABILIDADES.QUIMICA", attribute: 'intelligence', can_roll_on_null: false},
    teo: {label: "R3A.HABILIDADES.TEOLOGIA", attribute: 'intelligence', disadvantage_on_null: true},
}

R3A.stress = {
    'willpower': 'R3A.ESTRES.SITUACION_NATURAL',
    'essence': 'R3A.ESTRES.SITUACION_SOBRENATURAL'
}
R3A.stress_level = {
    0: "",
    1: "R3A.ESTRES.ESTADIO.LEVE",
    2: "R3A.ESTRES.ESTADIO.MODERADO",
    3: "R3A.ESTRES.ESTADIO.GRAVE",
    4: "R3A.ESTRES.ESTADIO.CRISIS"
}
R3A.remove_stress_difficulty = {
    1: 17,
    2: 21,
    3: 25
}

R3A.weapon_types = {
    'hands': 'R3A.ARMAS.TIPOS.SIN_ARMAS',
    'melee': 'R3A.ARMAS.TIPOS.MELEE',
    'handgun': 'R3A.ARMAS.TIPOS.ARMA_CORTA',
    'shotgun': 'R3A.ARMAS.TIPOS.ESCOPETA',
    'submachinegun': 'R3A.ARMAS.TIPOS.SUBFUSIL',
    'rifle': 'R3A.ARMAS.TIPOS.FUSIL',
    'distance': 'R3A.ARMAS.TIPOS.ARMAS_A_DISTANCIA',
}

R3A.spell_types = {
    'spell': 'R3A.MAGIA.TIPOS.CONJURO',
    'ritual': 'R3A.MAGIA.TIPOS.RITUAL'
}

R3A.ranged_weapons = ['handgun', 'shotgun', 'submachinegun', 'rifle']
R3A.burst_weapons = ['submachinegun', 'rifle']
export default R3A
