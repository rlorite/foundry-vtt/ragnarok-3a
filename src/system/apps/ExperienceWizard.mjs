import {sortObject} from "../utils.mjs";
import RagnarokItem from "../item/item.mjs";

export default class ExperienceWizard extends FormApplication {

    static get pages() {
        return {
            PAGE_NONE: -1,
            PAGE_NEW_SKILL: 1,
            PAGE_UPGRADE_SKILL: 2,
            PAGE_SPECS: 3,
            PAGE_UPGRADE_ATTRIBUTE: 4,
            PAGE_NEW_ADVANTAGE: 5,
            PAGE_BUY_PC: 6,
            PAGE_BUY_PL: 7
        }
    }

    static get defaultOptions() {
        return {
            ...super.defaultOptions,
            classes: ['experience-wizard'],
            width: 500,
            height: 500,
            closeOnSubmit: false,
            template: 'systems/ragnarok-3a/templates/apps/experience-wizard.hbs',
            dragDrop: [{dropSelector: '.advantage-list'}]
        }
    }

    async _updateObject(event, formData) {
        if ('action' in event.submitter?.dataset) {
            this[event.submitter.dataset.action](event.submitter, formData)
        }
        if ('page' in event.submitter?.dataset) {
            this.object.currentPage = Number(event.submitter.dataset.page)
        }
        if (event.submitter.dataset.button === 'save') {
            if (this.object.currentPage === ExperienceWizard.pages.PAGE_NONE) {
                return await this.save();
            }
            this.object.currentPage = ExperienceWizard.pages.PAGE_NONE
        }
        this.render(true)
    }


    getData(options = {}) {
        const context = super.getData(options);

        context.pages = ExperienceWizard.pages
        context.page = this.object.currentPage
        context.skills_purchased = this.object.skills_purchased.reduce((acc, skillId) => {
            acc[skillId] = this.object.actor.system.skills[skillId].label
            return acc
        }, {})
        const skills = this.object.actor.system.skills
        const sortedSkills = sortObject(skills)
        context.skills = Object.keys(sortedSkills).reduce((acc, skillId) => {
            const skill = skills[skillId]
            const attribute = this.object.actor.system.attributes[skill.attribute]
            const attributeUpgrades = this.object.attributes_upgraded[skill.attribute] ?? []
            const attributeLevel = attribute.value + attributeUpgrades.length
            const upgrades = this.object.skills_upgraded[skillId] ?? []
            const skillLevel = (skill.value || 1) + upgrades.length
            if (skill.value === 0 && !Object.keys(context.skills_purchased).includes(skillId)) {
                acc.at_zero[skillId] = skill.label
            } else if (skillLevel < attributeLevel) {
                acc.upgrade[skillId] = `${game.i18n.localize(skill.label)} [${game.i18n.localize("R3A.EXPERIENCIA.ACTUAL")}: ${skillLevel}]`
            }
            if (skillLevel >= 5) {
                acc.spec[skillId] = skill.label
            }
            return acc
        }, {at_zero: {}, upgrade: {}, spec: {}})

        const sortedAttributes = sortObject(this.object.actor.system.attributes, CONFIG.R3A.attributes)
        context.attributes = Object.keys(sortedAttributes).reduce((acc, attributeId) => {
            const attribute = sortedAttributes[attributeId]
            const upgrades = this.object.attributes_upgraded[attributeId] ?? []
            const attributeLevel = attribute.value + upgrades.length
            if (attributeLevel < 10) {
                acc[attributeId] = `${game.i18n.localize(CONFIG.R3A.attributes[attributeId])} [${game.i18n.localize("R3A.EXPERIENCIA.ACTUAL")}: ${attributeLevel}]`
            }
            return acc
        }, {})

        context.spent = this.object.spent + this.object.pcs.cost + this.object.pls.cost
        return context
    }

    async _onDrop(event) {
        try {
            const dataList = JSON.parse(event.dataTransfer.getData('text/plain'))
            if (dataList.type === "Item") {
                const item = await fromUuid(dataList.uuid)
                if (item.type === 'advantage') {
                    this.object.advantages_purchased.push(item)
                    this.object.spent += item.system.cost
                    this.render(true)
                }
            }
        } catch (err) {
            console.log(err)
        }
    }

    static create(actor) {
        new ExperienceWizard({
            actor,
            currentPage: ExperienceWizard.pages.PAGE_NONE,
            spent: 0,
            pcs: {
                value: 0,
                cost: 0
            },
            pls: {
                value: 0,
                cost: 0
            },
            advantages_purchased: [],
            skills_purchased: [],
            skills_upgraded: {},
            attributes_upgraded: {},
            skill_specs: []
        }).render(true)
    }

    newSkill(button, data) {
        this.object.spent += 5
        this.object.skills_purchased.push(data.skill)
    }

    removeSkill(button, data) {
        const skillId = button.dataset.skill
        if (this.object.skills_purchased.includes(skillId)) {
            this.object.skills_purchased = this.object.skills_purchased.filter(id => id !== skillId)
            this.object.spent -= 5
        }
    }

    buyPcs(button, data) {
        this.object.pcs.cost = 0
        this.object.pcs.value = Number(data.pcs)
        let actualPcs = this.object.actor.system.cp
        for (let i = 1; i <= this.object.pcs.value; i++) {
            this.object.pcs.cost += actualPcs * 3
            actualPcs++
        }
    }

    buyPls(button, data) {
        this.object.pls.value = data.pls
        this.object.pls.cost = data.pls * 5
    }

    specSkill(button, data) {
        if (data['spec.value']) {
            this.object.skill_specs.push({
                skill: data['spec.skill'],
                value: data['spec.value'],
            })
            this.object.spent += 4
        }
    }

    removeSpec(button, data) {
        const index = Number(button.dataset.spec)
        this.object.skill_specs.splice(index, 1)
        this.object.spent -= 4
    }

    removeAdvantage(button, data) {
        this.object.advantages_purchased = this.object.advantages_purchased.filter(advantage => {
            if (advantage.id === button.dataset.advantage) {
                this.object.spent -= advantage.system.cost
                return false
            }
            return true
        })
    }

    upgradeSkill(button, data) {
        const skill = this.object.actor.system.skills[data.skill]
        const levelsUpgraded = this.object.skills_upgraded[data.skill] ?? []
        const skillLevel = (skill.value || 1) + levelsUpgraded.length
        const cost = skillLevel * 2
        const upgradedData = this.object.skills_upgraded[data.skill] ??= []
        upgradedData.push({
            label: skill.label,
            level: {
                before: skillLevel,
                after: skillLevel + 1
            },
            cost
        })
        this.object.skills_upgraded[data.skill] = upgradedData
        this.object.spent += cost
    }

    downgradeSkill(button, data) {
        const skillUpgrades = this.object.skills_upgraded[button.dataset.skill]
        const removedUpgrade = skillUpgrades.pop()
        this.object.spent -= removedUpgrade.cost
    }

    upgradeAttribute(button, data) {
        const attribute = this.object.actor.system.attributes[data.attribute]
        const levelsUpgraded = this.object.attributes_upgraded[data.attribute] ?? []
        const attributeLevel = (attribute.value || 1) + levelsUpgraded.length
        const cost = attributeLevel * 5
        levelsUpgraded.push({
            label: CONFIG.R3A.attributes[data.attribute],
            level: {
                before: attributeLevel,
                after: attributeLevel + 1
            },
            cost
        })
        this.object.attributes_upgraded[data.attribute] = levelsUpgraded
        this.object.spent += cost
    }


    downgradeAttribute(button, data) {
        const attributeUpgrades = this.object.attributes_upgraded[button.dataset.attribute]
        const removedUpgrade = attributeUpgrades.pop()
        this.object.spent -= removedUpgrade.cost
    }

    async save() {
        const actor = this.object.actor
        const xp = actor.system.xp
        const spent = this.object.spent + this.object.pcs.cost + this.object.pls.cost
        if (xp < spent) {
            ui.notifications.error("R3A.EXPERIENCIA.AVISO_PUNTOS", {localize: true})
            return false;
        }
        const updates = {}

        updates['system.xp'] = xp - spent
        this.object.skills_purchased.forEach(skillId => {
            updates[`system.skills.${skillId}.value`] = 1
        })
        Object.keys(this.object.skills_upgraded).forEach(skillId => {
            const skill = actor.system.skills[skillId]
            const upgrades = this.object.skills_upgraded[skillId]
            let newLevel = skill.value + upgrades.length
            if (this.object.skills_purchased.includes(skillId)) {
                newLevel++
            }
            updates[`system.skills.${skillId}.value`] = newLevel
        })

        Object.keys(this.object.attributes_upgraded).forEach(attributeId => {
            const attribute = actor.system.attributes[attributeId]
            const upgrades = this.object.attributes_upgraded[attributeId]
            updates[`system.attributes.${attributeId}.value`] = attribute.value + upgrades.length
        })

        this.object.skill_specs.forEach(spec => {
            const skill = actor.system.skills[spec.skill]
            skill.specializations.push(spec.value)
            updates[`system.skills.${spec.skill}.specializations`] = skill.specializations
        })

        if (this.object.pcs.value) {
            updates[`system.cp`] = actor.system.cp + this.object.pcs.value
        }
        if (this.object.pls.value) {
            updates[`system.lp.increments`] = actor.system.lp.increments + this.object.pls.value

        }
        for (const advantage of this.object.advantages_purchased) {
            await actor.createEmbeddedDocuments("Item", [advantage]);
        }
        await actor.update(updates)
        this.close()
    }
}
