export default function () {
    game.settings.register("ragnarok-3a", "autoApplyStress", {
        name: "SETTINGS.AUTO_APPLY_STREES.NAME",
        hint: "SETTINGS.AUTO_APPLY_STREES.HINT",
        scope: 'client',
        type: Boolean,
        config: true,
        default: true
    })
    game.settings.register("ragnarok-3a", "autoApplyDamage", {
        name: "SETTINGS.AUTO_APPLY_DAMAGE.NAME",
        hint: "SETTINGS.AUTO_APPLY_DAMAGE.HINT",
        scope: 'client',
        type: Boolean,
        config: true,
        default: true
    })
    game.settings.register("ragnarok-3a", "autoConsumeLP", {
        name: "SETTINGS.AUTO_CONSUME_LP.NAME",
        hint: "SETTINGS.AUTO_CONSUME_LP.HINT",
        scope: 'client',
        type: Boolean,
        config: true,
        default: true
    })

}
