import D20Roll from "./d20-roll.mjs";

export default class StressRoll extends D20Roll {

    constructor(formula, data, options, actor) {
        super("1d20 + @attr_mod + @skill_mod - @stress_mod", data, {
            flavor: game.i18n.localize("R3A.TIRADAS.PROMPT_ESTRES"),
            ...options
        }, actor);
    }

    get dialogTemplate() {
        return 'systems/ragnarok-3a/templates/dialog/stress-roll-dialog.hbs'
    }

    async renderContent(data = {}) {
        if (this.total < this.options.targetValue) {
            if (game.settings.get('ragnarok-3a', 'autoApplyStress')) {
                this.addNotification({
                    'type': 'info',
                    'text': "R3A.ESTRES.ESTRES_APLICADO"
                })
                await this.actor.applyStress()
            } else {
                this.addAction({
                    name: 'apply-stress',
                    actor: this.actor,
                    label: game.i18n.format("R3A.ESTRES.APLICAR_ESTRES", {
                        name: this.actor.name
                    })
                })
            }
        }
        return super.renderContent(data);
    }

    async configureDialog(data = {}, options = {}) {
        return super.configureDialog({
            title: game.i18n.localize("R3A.TIRADAS.PROMPT_ESTRES"),
            attributes: CONFIG.R3A.stress,
            defaultAttribute: this.options.defaultAttribute || 'willpower'
        });
    }
}
