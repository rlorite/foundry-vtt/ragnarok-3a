import {D20Roll} from "./dice.mjs";

export default class SpellRoll extends D20Roll {

    /**
     * Create a Dialog prompt used to configure evaluation of an existing D20Roll instance.
     * @param {object} data                     Dialog configuration data
     * @param {object} options                  Additional Dialog customization options
     * @param {boolean} [options.chooseModifier]   Choose which ability modifier should be applied to the roll?
     * @param {boolean} [options.fastForward]
     * @param {RagnarokItem} [options.spell]
     * @param {Event} [options.event]
     * @param {RagnarokActor} actor
     */
    static async roll(data, options, actor = null) {
        const roll = new this("1d20 + @attributes.willpower.value + @skills.con.value", data, {
            ...options,
            chooseModifier: false,
            flavor: game.i18n.format("R3A.TIRADAS.PROMPT_CONJURO", {
                spell: options.spell.name
            }),
        }, actor)
        await roll.configureRoll({async: true})
        return roll
    }

    async hookChatCardActions(data) {
        const spell = this.options.spell
        const lp_cost = this.critical ? 0
            : Math.max(1, spell.system.lp - this.successLevel)

        this.addAction({
            name: 'roll-spell-stress',
            label: "R3A.MAGIA.TIRADA_ESTRES",
            actor: this.actor,
            dataset: {
                difficulty: spell.system.stress_difficulty
            }
        })
        if (this.critical) {
            await this.actor.applyLPIncrement();
            ui.notifications.info("R3A.MAGIA.INCR_PL", {localize: true})
        } else {
            if (game.settings.get('ragnarok-3a', 'autoConsumeLP')) {
                await this.actor.consumeLightPoints(lp_cost, this.critical)
                this.addNotification({
                    type: 'info',
                    text: game.i18n.format("R3A.MAGIA.PL_DESCONTADOS", {
                        lp: lp_cost
                    })
                })
            } else {
                this.addAction({
                    name: 'consume-lp',
                    label: game.i18n.format("R3A.MAGIA.DESCONTAR_LP", {
                        lp: lp_cost
                    }),
                    actor: this.actor,
                    dataset: {
                        lp: lp_cost
                    }
                })
            }
        }


    }

}
