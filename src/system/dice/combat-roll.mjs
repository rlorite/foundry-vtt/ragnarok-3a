import D20Roll from "./d20-roll.mjs";
import {RagnarokActor} from "../actor/actor.js";

export default class CombatRoll extends D20Roll {

    constructor(formula, data, options, actor) {
        options.target = options.target instanceof Token
            ? options.target?.actor.uuid
            : options.target
        options.dpa ??= options.weapon?.system.dpa ?? 1
        super("1d20 + @attr_mod + @skill_mod", data, {
            flavor: game.i18n.format("R3A.TIRADAS.PROMPT_COMBATE", {
                weapon: options.weapon?.name
            }),
            ...options
        }, actor);
    }


    get dialogTemplate() {
        return 'systems/ragnarok-3a/templates/dialog/combat-roll-dialog.hbs'
    }

    get messageTemplate() {
        return 'systems/ragnarok-3a/templates/chat/combat-roll.hbs'
    }

    async configureDialog(data = {}, options = {}) {
        const target = await RagnarokActor.fromUuid(this.options.target)
        const difficulty = target?.system.da || 17
        return super.configureDialog({
            title: game.i18n.format("R3A.TIRADAS.PROMPT_COMBATE", {
                weapon: this.options.weapon?.name
            }),
            difficulty: difficulty + (this.options?.incrDifficulty || 0),
            attributes: CONFIG.R3A.stress,
            target,
            weapon: this.options.weapon
        });
    }

    async renderContent(data = {}) {
        const target = await RagnarokActor.fromUuid(this.options.target)
        if (this.options.dpa > 1) {
            this.addAction({
                name: 'attack-dpa',
                actor: this.actor,
                label: game.i18n.format("R3A.COMBATE.DISPARAR_NUEVAMENTE", {
                    dpa: this.options.dpa
                }),
                dataset: {
                    dpa: this.options.dpa,
                    difficulty: this.options.incrDifficulty,
                    'target-id': this.options.target,
                    'weapon-id': this.options.weapon.id
                }
            })
        }
        if (this.success) {
            this.addAction({
                name: 'roll-damage',
                actor: this.actor,
                label: game.i18n.format("R3A.COMBATE.DAMAGE", {
                    damage: this.options.weapon.system.damage
                }),
                dataset: {
                    'item-id': this.options.weapon.id,
                    'target-id': this.options.target
                }
            })
        }
        return await super.renderContent({
            ...data,
            difficulty: this.options.targetValue,
            target,
        })
    }
}
