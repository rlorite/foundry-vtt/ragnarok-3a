import {D20Roll} from "./dice.mjs";

export default class PsychicRoll extends D20Roll {

    /**
     * Create a Dialog prompt used to configure evaluation of an existing D20Roll instance.
     * @param {object} data                     Dialog configuration data
     * @param {object} options                  Additional Dialog customization options
     * @param {boolean} [options.chooseModifier]   Choose which ability modifier should be applied to the roll?
     * @param {boolean} [options.fastForward]
     * @param {RagnarokItem} [options.spell]
     * @param {Event} [options.event]
     * @param {RagnarokActor} actor
     */
    static async roll(data, options, actor = null) {
        const roll = new this("1d20 + @attributes.willpower.value + @skills.con.value", data, {
            ...options,
            chooseModifier: false,
            flavor: game.i18n.format("R3A.TIRADAS.PROMPT_PARANORMAL", {
                psychic: options.psychic.name
            }),
        }, actor)
        await roll.configureRoll({async: true})
        return roll
    }

    async hookChatCardActions(data) {
        const psychic = this.options.psychic

        if (this.success || this.fumble) {
            this.addAction({
                name: 'roll-spell-stress',
                label: "R3A.MAGIA.TIRADA_ESTRES",
                actor: this.actor,
                dataset: {
                    difficulty: psychic.system.stress_difficulty
                }
            })
            if (!this.critical) {
                this.addAction({
                    name: 'roll-psychic-damage',
                    actor: this.actor,
                    label: game.i18n.format("R3A.COMBATE.DAMAGE", {
                        damage: `${this.fumble ? 2 : 1}d6`
                    }),
                    dataset: {
                        'item-id': this.options.psychic.id,
                        'target-id': this.actor.uuid,
                        'fumble': Number(this.fumble)
                    }
                })
            }
        }
    }

}
