import RagnarokRoll from "./roll.mjs";
import {RagnarokActor} from "../actor/actor.js";

export default class DamageRoll extends RagnarokRoll {
    constructor(formula, data, options, actor) {
        options.target = options.target instanceof Token
            ? options.target?.actor.uuid
            : options.target
        super(formula, data, options, actor);
    }

    /**
     * Create a Dialog prompt used to configure evaluation of an existing D20Roll instance.
     * @param {string} damage                   Damage formula
     * @param {object} data                     Dialog configuration data
     * @param {object} options                  Additional Dialog customization options
     * @param {boolean} [options.chooseModifier]   Choose which ability modifier should be applied to the roll?
     * @param {boolean} [options.fastForward]
     * @param {Event} [options.event]
     * @param {RagnarokActor} actor
     */
    static async roll(damage, data, options, actor = null) {
        const roll = new this(damage, data, {
            ...options,
        }, actor)
        await roll.configureRoll()
    }


    async renderContent(data = {}) {
        const target = await RagnarokActor.fromUuid(this.options.target)
        if (target) {
            const canUserModifyTarget = target.canUserModify(game.user, "update");
            if (canUserModifyTarget && game.settings.get('ragnarok-3a', 'autoApplyDamage')) {
                const damage = await target.applyDamage(this.total)
                this.addNotification({
                    'type': 'info',
                    'text': game.i18n.format("R3A.COMBATE.DANO_APLICADO", {
                        actor: target.name,
                        damage
                    })
                })
            } else {
                this.addAction({
                    name: 'apply-damage',
                    actor: target,
                    label: game.i18n.format("R3A.COMBATE.APLICAR_DANO", {
                        name: target.name,
                        damage: this.total
                    }),
                    dataset: {
                        damage: this.total
                    }
                })
            }
        }

        return super.renderContent(data);
    }
}
