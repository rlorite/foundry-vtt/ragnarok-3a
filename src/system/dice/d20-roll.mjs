import RagnarokRoll from "./roll.mjs";

/**
 * A type of Roll specific to a d20-based check, save, or attack roll in the 5e system.
 * @param {string} formula                       The string formula to parse
 * @param {object} data                          The data object against which to parse attributes within the formula
 * @param {object} [options={}]                  Extra optional arguments which describe or modify the D20Roll
 * @param {number} [options.advantageMode]       What advantage modifier to apply to the roll (none, advantage,
 *                                               disadvantage)
 * @param {number} [options.template]            The HTML template used to display the chat message.
 * @param {number} [options.critical]            The value of d20 result which represents a critical success
 * @param {number} [options.fumble]              The value of d20 result which represents a critical failure
 * @param {(number)} [options.targetValue]       Assign a target value against which the result of this roll should be
 *                                               compared
 */
export default class D20Roll extends RagnarokRoll {
    constructor(formula, data, options, actor) {
        super(formula, data, {
            ...options,
            advantageMode: D20Roll.determineAdvantageMode(options)
        }, actor);
        if (!this.options.configured) this.configureModifiers();
    }

    /* -------------------------------------------- */

    /**
     * Determine whether a d20 roll should be fast-forwarded, and whether advantage or disadvantage should be applied.
     * @param {object} [options]
     * @param {Event} [options.event]                               The Event that triggered the roll.
     * @param {boolean} [options.advantage]                         Is something granting this roll advantage?
     * @param {boolean} [options.disadvantage]                      Is something granting this roll disadvantage?
     * @returns D20Roll.ADV_MODE  Whether the roll is fast-forwarded, and its advantage
     *                                                              mode.
     */
    static determineAdvantageMode({event, advantage = false, disadvantage = false, fastForward} = {}) {
        let advantageMode = this.ADV_MODE.NORMAL;
        if (advantage || event?.altKey) advantageMode = this.ADV_MODE.ADVANTAGE;
        else if (disadvantage || event?.ctrlKey || event?.metaKey) advantageMode = this.ADV_MODE.DISADVANTAGE;
        return advantageMode
    }

    /* -------------------------------------------- */

    /**
     * Advantage mode of a 5e d20 roll
     * @enum {number}
     */
    static ADV_MODE = {
        NORMAL: 0,
        ADVANTAGE: 1,
        DISADVANTAGE: -1
    }

    /* -------------------------------------------- */

    /**
     * The HTML template path used to configure evaluation of this Roll
     * @type {string}
     */
    get dialogTemplate() {
        return 'systems/ragnarok-3a/templates/dialog/roll-dialog.hbs'
    }

    get messageTemplate() {
        return 'systems/ragnarok-3a/templates/chat/d20-roll.hbs'
    }

    /**
     * Does this roll start with a d20?
     * @type {boolean}
     */
    get validD20Roll() {
        return (this.terms[0] instanceof Die) && (this.terms[0].faces === 20);
    }

    /* -------------------------------------------- */

    /**
     * A convenience reference for whether this D20Roll has advantage
     * @type {boolean}
     */
    get hasAdvantage() {
        return this.options.advantageMode === D20Roll.ADV_MODE.ADVANTAGE;
    }

    /* -------------------------------------------- */

    /**
     * A convenience reference for whether this D20Roll has disadvantage
     * @type {boolean}
     */
    get hasDisadvantage() {
        return this.options.advantageMode === D20Roll.ADV_MODE.DISADVANTAGE;
    }

    get success() {
        return this.total >= this.options.targetValue
    }

    get successLevel() {
        return this.total - this.options.targetValue
    }

    get critical() {
        return this.dice[0].total === 20
    }

    get fumble() {
        return this.dice[0].total === 1
    }

    /* -------------------------------------------- */
    /*  D20 Roll Methods                            */

    /* -------------------------------------------- */

    /**
     * Apply optional modifiers which customize the behavior of the d20term
     * @private
     */
    configureModifiers() {
        if (!this.validD20Roll) return;

        const d20 = this.terms[0]
        d20.modifiers = []
        d20.number = 1
        if (this.hasAdvantage) {
            d20.number = 2
            d20.modifiers.push("kh")
            d20.options.advantage = true
        } else if (this.hasDisadvantage) {
            d20.number = 2
            d20.modifiers.push("kl")
            d20.options.disadvantage = true
        }

        // Assign critical and fumble thresholds
        if (this.options.critical) d20.options.critical = this.options.critical;
        if (this.options.fumble) d20.options.fumble = this.options.fumble;
        if (this.options.targetValue) d20.options.target = this.options.targetValue;


        // Re-compile the underlying formula
        this._formula = this.constructor.getFormula(this.terms);

        // Mark configuration as complete
        this.options.configured = true;
    }

    /* -------------------------------------------- */
    /*  Configuration Dialog                        */

    /* -------------------------------------------- */

    /**
     * Create a Dialog prompt used to configure evaluation of an existing D20Roll instance.
     * @param {object} data                     Dialog configuration data
     * @param {string} [data.title]             The title of the shown dialog window
     * @param {number} [data.attributes]        List of attributes to choose
     * @param {number} [data.defaultRollMode]   The roll mode that the roll mode select element should default to
     * @param {number} [data.defaultAction]     The button marked as default
     * @param {boolean} [data.chooseModifier]   Choose which ability modifier should be applied to the roll?
     * @param {string} [data.defaultAttribute]  For tool rolls, the default attribute modifier applied to the roll
     * @param {string} [data.template]          A custom path to an HTML template to use instead of the default
     * @param {object} options                  Additional Dialog customization options
     * @returns {Promise<D20Roll|null>}         A resulting D20Roll object constructed with the dialog, or null if the
     *                                          dialog was closed
     */
    async configureDialog(data = {}, options = {}) {
        const content = await renderTemplate(this.dialogTemplate, {
            defaultRollMode: data.defaultRollMode || game.settings.get("core", "rollMode"),
            rollModes: CONFIG.Dice.rollModes,
            difficulty: CONFIG.R3A.difficulty,
            defaultDifficulty: this.options.targetValue ?? 17,
            chooseModifier: this.options.chooseModifier ?? true,
            defaultAttribute: this.options.defaultAttribute,
            specChecked: this.options.specChecked ?? false,
            attributes: CONFIG.R3A.attributes,
            ...data
        })

        let defaultButton = "normal"
        if (this.options.advantageMode === D20Roll.ADV_MODE.ADVANTAGE) defaultButton = "advantage"
        if (this.options.advantageMode === D20Roll.ADV_MODE.DISADVANTAGE) defaultButton = "disadvantage"

        return new Promise(resolve => new Dialog({
            title: this.options.flavor || '',
            content,
            buttons: {
                advantage: {
                    label: game.i18n.localize("R3A.TIRADAS.VENTAJA"),
                    callback: html => resolve(this._onDialogSubmit(html, D20Roll.ADV_MODE.ADVANTAGE))
                },
                normal: {
                    label: game.i18n.localize("R3A.TIRADAS.NORMAL"),
                    callback: html => resolve(this._onDialogSubmit(html, D20Roll.ADV_MODE.NORMAL))
                },
                disadvantage: {
                    label: game.i18n.localize("R3A.TIRADAS.DESVENTAJA"),
                    callback: html => resolve(this._onDialogSubmit(html, D20Roll.ADV_MODE.DISADVANTAGE))
                }
            },
            default: defaultButton,
            close: () => resolve(null)
        }, options).render(true));
    }

    /**
     * Handle submission of the Roll evaluation configuration Dialog
     * @param {jQuery} html            The submitted dialog content
     * @param {number} advantageMode   The chosen advantage mode
     * @returns {D20Roll}              This damage roll.
     * @private
     */
    _onDialogSubmit(html, advantageMode) {
        const form = html[0].querySelector("form")

        if (form.bonus.value) {
            const bonus = new Roll(form.bonus.value, this.data);
            if (!(bonus.terms[0] instanceof OperatorTerm)) this.terms.push(new OperatorTerm({operator: "+"}));
            this.terms = this.terms.concat(bonus.terms);
        }

        if (form.spec?.checked) {
            this.terms.push(
                new OperatorTerm({operator: '+'}),
                new NumericTerm({number: 2})
            )
        }

        if (form.attribute?.value) {
            const attribute = this.data.attributes[form.attribute.value]
            this.terms = this.terms.flatMap(term => {
                if (term.term === "@attr_mod") return new NumericTerm({number: attribute.value})
                return term;
            })
            this.options.flavor += ` (${game.i18n.localize(CONFIG.R3A.attributes[form.attribute.value])})`;
        }
        this.options.advantageMode = advantageMode;
        this.formToOptions(form);
        this.configureModifiers();
        return this;
    }

    formToOptions(form) {
        this.options.targetValue = Number(form.difficulty.value);
        this.options.rollMode = form.rollMode.value;
    }

    /** @inheritDoc */
    async toMessage(messageData = {}, options = {}) {
        if (!this._evaluated) await this.evaluate({async: true});
        messageData.flavor ??= this.options.flavor
        if (this.hasAdvantage) messageData.flavor += ` [${game.i18n.localize("R3A.TIRADAS.VENTAJA")}]`
        if (this.hasDisadvantage) messageData.flavor += ` [${game.i18n.localize("R3A.TIRADAS.DESVENTAJA")}]`
        return super.toMessage(messageData, options);
    }

    async renderContent(data = {}) {

        const classes = []
        if (this.success) classes.push('success')
        else classes.push('failure')


        if (Math.abs(this.successLevel) >= 10) {
            classes.push('critical')
        }

        return super.renderContent({
            success_level: {
                value: Math.abs(this.successLevel),
                label: this.successLevel < 0 ? 'R3A.TIRADAS.CATEGORIA_FRACASO' : 'R3A.TIRADAS.CATEGORIA_EXITO'
            },
            difficulty: CONFIG.R3A.difficulty[this.options.targetValue],
            classes: classes.join(" "),
            ...data,
        })
    }


    static isFF({event, fastForward}) {
        return fastForward ?? (event?.shiftKey || event?.altKey || event?.ctrlKey || event?.metaKey);
    }

    async configureRoll(options = {}) {
        if (!this.options.fastForward) {
            const configured = await this.configureDialog();
            if (configured === null) return null;
        }
        await super.configureRoll(options)
    }

    /**
     * Create a Dialog prompt used to configure evaluation of an existing D20Roll instance.
     * @param {object} data                     Dialog configuration data
     * @param {object} options                  Additional Dialog customization options
     * @param {boolean} [options.chooseModifier]   Choose which ability modifier should be applied to the roll?
     * @param {boolean} [options.fastForward]
     * @param {Event} [options.event]
     * @param {RagnarokActor} actor
     */
    static async roll(data, options, actor = null) {
        options.fastForward = this.isFF(options);
        options.chooseModifier = options.chooseModifier ?? true
        if (options.chooseModifier && !options.fastForward) {
            data.attr_mod = "@attr_mod"
        }
        const roll = new this("1d20 + @attr_mod + @skill_mod", data, {
            ...options,
            advantageMode: this.determineAdvantageMode(options)
        }, actor)
        await roll.configureRoll({async: true})
        return roll
    }


}
