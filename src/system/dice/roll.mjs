export default class RagnarokRoll extends Roll {

    constructor(formula, data, options, actor = null) {
        super(formula, data, options);
        this.actor = actor
        this.notifications = []
        this.actions = []
    }

    get messageTemplate() {
        return 'systems/ragnarok-3a/templates/chat/roll.hbs'
    }

    async toMessage(messageData = {}, options = {}) {
        if (!this._evaluated) await this.evaluate({async: true});
        messageData.flavor = messageData.flavor || this.options.flavor

        messageData.content = await this.renderContent();
        options.rollMode = options.rollMode ?? this.options.rollMode
        return super.toMessage(messageData, options);
    }

    async hookChatCardActions(data) {}
    async renderContent(data = {}) {
        await this.hookChatCardActions(data)
        return renderTemplate(this.messageTemplate, {
            actor: this.actor,
            actions: this.actions,
            notifications: this.notifications,
            tooltip: await this.getTooltip(),
            total: this.total,
            formula: this.formula,
            ...data
        })
    }

    async configureRoll(options = {}){
        await super.roll({
            ...options,
            async: true
        });
        await this.toMessage({})
    }

    /**
     * Create a roll notification for the chat message
     * @param {object} notification             Chat notification
     * @param {string} [notification.type]      Notification type for style rendering
     * @param {string} [notification.text]      Localizable notification content
     */
    addNotification(notification) {
        this.notifications.push(notification)
    }

    /**
     * Create a roll action for the chat message
     * @param {object} action                 Chat action
     * @param {string} [action.name]          Action name for style rendering
     * @param {string} [action.label]         Localizable action label
     * @param {RagnarokActor} [action.actor]  Actor to apply action logic
     * @param {Object} [action.dataset]       Options for custom dataset
     */
    addAction(action) {
        this.actions.push(action)
    }
}
