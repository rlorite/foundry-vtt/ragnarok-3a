import D20Roll from "./d20-roll.mjs";

export default class RemoveStressRoll extends D20Roll {

    constructor(formula, data, options, actor) {
        options.fastForward = true
        options.targetValue = CONFIG.R3A.remove_stress_difficulty[actor.system.stress.pet]
        super("1d20 + @attr_mod + @skill_mod", data, {
            flavor: game.i18n.localize("R3A.TIRADAS.PROMPT_REMOVE_ESTRES"),
            ...options
        }, actor);
    }

    async renderContent(data = {}) {
        let stress = 0
        if (this.critical) {
            stress = 3
        }
        if (this.fumble) {
            stress = -1
        }
        if (this.success && !this.critical) {
            stress = 1
        }
        if (stress > 0) {
            if (game.settings.get('ragnarok-3a', 'autoApplyStress')) {
                this.addNotification({
                    'type': 'info',
                    'text': game.i18n.format("R3A.ESTRES.ESTRES_ELIMINADO", {stress})
                })
                await this.actor.removeStress(stress)
            } else {
                this.addAction({
                    name: 'remove-stress',
                    actor: this.actor,
                    label: game.i18n.format("R3A.ESTRES.ELIMINAR_ESTRES", {
                        name: this.actor.name,
                        stress
                    }),
                    dataset: {
                        stress
                    }
                })
            }
        } else if (stress < 0) {
            if (game.settings.get('ragnarok-3a', 'autoApplyStress')) {
                this.addNotification({
                    'type': 'info',
                    'text': "R3A.ESTRES.ESTRES_APLICADO"
                })
                await this.actor.applyStress()
            } else {
                this.addAction({
                    name: 'apply-stress',
                    actor: this.actor,
                    label: game.i18n.format("R3A.ESTRES.APLICAR_ESTRES", {
                        name: this.actor.name
                    })
                })
            }
        }
        return super.renderContent(data);
    }

    async configureDialog(data = {}, options = {}) {
        return super.configureDialog({
            title: game.i18n.localize("R3A.TIRADAS.PROMPT_ESTRES"),
            attributes: CONFIG.R3A.stress,
            defaultAttribute: this.options.defaultAttribute || 'willpower'
        });
    }
}
