export function sortObject(object, labels = undefined) {
    return Object.keys(object)
        .sort((a, b) => {
            const localizedA = game.i18n.localize(object[a].label ?? labels[a])
            const localizedB = game.i18n.localize(object[b].label ?? labels[b])
            if (localizedA < localizedB) {
                return -1
            }
            if (localizedA > localizedB) {
                return 1
            }
            return 0
        }).reduce((acc, key) => {
            acc[key] = object[key];
            return acc;
        }, {})
}
