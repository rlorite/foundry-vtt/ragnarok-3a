import CharacterData from "./character.mjs";
import NpcData from "./npc.mjs";


export default {
    character: CharacterData,
    npc: NpcData
}
