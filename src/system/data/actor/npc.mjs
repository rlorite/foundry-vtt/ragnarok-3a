import {BaseActorData} from "./base.mjs";

export default class NpcData extends BaseActorData {
    static defineSchema() {
        return {
            ...super.defineSchema(),
            stress_difficulty: new foundry.data.fields.NumberField({
                initial: 0, choices: CONFIG.R3A.actor_stress_difficulty
            }),
            description: new foundry.data.fields.HTMLField()
        }
    }
}
