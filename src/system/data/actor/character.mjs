import {BaseActorData} from "./base.mjs";

export default class CharacterData extends BaseActorData {
    static defineSchema() {
        const fields = foundry.data.fields
        return {
            ...super.defineSchema(),
            personal: new fields.SchemaField({
                age: new fields.NumberField({required: false}),
                profession: new fields.StringField({required: false, label: 'R3A.PROFESION'}),
                earnings: new fields.NumberField({required: false}),
                spending_level: new fields.NumberField({required: false}),
                savings: new fields.NumberField({required: false}),
                nationality: new fields.StringField({required: false}),
                birthdate: new fields.StringField({required: false}),
                height: new fields.StringField({required: false}),
                weigth: new fields.StringField({required: false}),
                physical_traits: new fields.StringField({required: false}),
                psychological_traits: new fields.StringField({required: false}),
            }),
            xp: new fields.NumberField({initial: 0, integer: true}),
            stress: new fields.SchemaField({
                pet: new fields.NumberField({nullable: false, integer: true, initial: 0}),
                pep: new fields.NumberField({nullable: false, integer: true, initial: 0, max: 4}),
                disorder: new fields.HTMLField({required: false})
            }),
            cp: new fields.NumberField({nullable: true, initial: 0, integer: true}),
        }
    }

}

