import {MappingField} from "../fields.mjs";

export class BaseActorData extends foundry.abstract.DataModel {

    static defineSchema() {
        const fields = foundry.data.fields
        return {
            motivation: new fields.SchemaField({
                points: new fields.NumberField({initial: 0, max: 3, min: 0}),
                description: new fields.StringField({required: false, label: 'R3A.MOTIVACION'})
            }),
            hp: new fields.SchemaField({
                value: new fields.NumberField({nullable: true, integer: true}),
                max: new fields.NumberField({nullable: true, integer: true})
            }),
            lp: new fields.SchemaField({
                value: new fields.NumberField({nullable: true, integer: true}),
                max: new fields.NumberField({nullable: true, integer: true}),
                increments: new fields.NumberField({nullable: true, integer: true, initial: 0})
            }),
            da: new fields.NumberField({nullable: true}),
            bd: new fields.StringField({nullable: false, initial: ''}),
            vad: new fields.NumberField({nullable: false, initial: 0}),
            attributes: new MappingField(new fields.SchemaField({
                value: new fields.NumberField({initial: 0, max: 10, min: 0})
            }), {initialKeys: CONFIG.R3A.attributes}),
            skills: new MappingField(new fields.SchemaField({
                label: new fields.StringField({required: true}),
                value: new fields.NumberField({initial: 0}),
                attribute: new fields.StringField({required: true, nullable: false, blank: false}),
                disadvantage_on_null: new fields.BooleanField({required: false, initial: false}),
                can_roll_on_null: new fields.BooleanField({required: false, initial: true}),
                has_description: new fields.BooleanField({required: false, initial: false}),
                description: new fields.StringField({required: false}),
                specializations: new fields.ArrayField(new fields.StringField(), {required: false})
            }), {initialKeys: CONFIG.R3A.skills, initialValue: this.initialSkillValue})
        }
    }


    static initialSkillValue(key, initial) {
        return {
            ...initial,
            ...CONFIG.R3A.skills[key]
        }
    }

    static migrateData(source) {
        for (let skill_id of Object.keys(source.skills)) {
            source.skills[skill_id] = {
                ...source.skills[skill_id],
                has_description: CONFIG.R3A.skills[skill_id]?.has_description ?? false
            }
        }
    }
}
