import SystemDataModel from "../system-data-model.mjs";
import EquippableItemTemplate from "./template/EquippableItemTemplate.mjs";
import PhysicalItemTemplate from "./template/PhysicalItemTemplate.mjs";

export default class ProtectionData extends SystemDataModel.mixin(EquippableItemTemplate, PhysicalItemTemplate) {
    static defineSchema() {
        return this.mergeSchema(super.defineSchema(), {
            vad: new foundry.data.fields.NumberField({required: true, integer: true, min: 1, label: "R3A.EQUIPO.VAD"}),
            dexterity_disadvantage: new foundry.data.fields.BooleanField({initial: false})
        })
    }
}
