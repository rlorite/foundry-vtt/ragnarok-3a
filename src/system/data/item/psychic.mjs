export default class PsychicData extends foundry.abstract.DataModel {
    static defineSchema() {
        return {
            type: new foundry.data.fields.StringField({initial: 'spell', choices: Object.keys(CONFIG.R3A.spell_types)}),
            time: new foundry.data.fields.StringField(),
            duration: new foundry.data.fields.StringField(),
            stress_difficulty: new foundry.data.fields.NumberField({
                integer: true, initial: 17, choices: Object.keys(CONFIG.R3A.spell_difficulty).map(d => Number(d))
            }),
            description: new foundry.data.fields.HTMLField()
        }
    }
}
