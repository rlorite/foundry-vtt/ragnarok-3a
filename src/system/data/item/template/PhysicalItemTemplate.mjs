export default class PhysicalItemTemplate extends foundry.abstract.DataModel {
    static defineSchema() {
        return {
            price: new foundry.data.fields.NumberField({
                initial: 0, min: 0, label: "R3A.EQUIPO.PRECIO"
            }),
            quantity: new foundry.data.fields.NumberField({
                integer: true, initial: 1, min: 0, label: "R3A.EQUIPO.Quantity"
            })
        }
    }
}
