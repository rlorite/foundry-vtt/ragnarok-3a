export default class EquippableItemTemplate extends foundry.abstract.DataModel {
    static defineSchema() {
        return {
            equipped: new foundry.data.fields.BooleanField({initial: false, label: "R3A.EQUIPO.EQUIPADO"})
        }
    }
}
