import TraitsData from "./trait.mjs";
import ProtectionData from "./protection.mjs";
import WeaponData from "./weapon.mjs";
import SpellData from "./spell.mjs";
import PsychicData from "./psychic.mjs";

export default {
    advantage: TraitsData,
    disadvantage: TraitsData,
    protection: ProtectionData,
    weapon: WeaponData,
    spell: SpellData,
    psychic: PsychicData
}
