export default class SpellData extends foundry.abstract.DataModel {
    static defineSchema() {
        return {
            type: new foundry.data.fields.StringField({initial: 'spell', choices: Object.keys(CONFIG.R3A.spell_types)}),
            time: new foundry.data.fields.StringField(),
            duration: new foundry.data.fields.StringField(),
            difficulty: new foundry.data.fields.NumberField({
                integer: true, initial: 17, choices: Object.keys(CONFIG.R3A.spell_difficulty)
                    .map(d => Number(d))
            }),
            stress_difficulty: new foundry.data.fields.NumberField({
                integer: true, initial: 17, choices: Object.keys(CONFIG.R3A.spell_difficulty).map(d => Number(d))
            }),
            lp: new foundry.data.fields.NumberField({integer: true}),
            components: new foundry.data.fields.StringField(),
            description: new foundry.data.fields.HTMLField()
        }
    }
}
