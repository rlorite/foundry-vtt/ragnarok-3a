export default class TraitsData extends foundry.abstract.DataModel {
    static defineSchema() {
        return {
            cost: new foundry.data.fields.NumberField({required: true, initial: 0, integer: true}),
            description: new foundry.data.fields.HTMLField()
        }
    }
}
