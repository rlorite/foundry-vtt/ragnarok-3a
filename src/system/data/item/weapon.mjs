import SystemDataModel from "../system-data-model.mjs";
import PhysicalItemTemplate from "./template/PhysicalItemTemplate.mjs";

export default class WeaponData extends SystemDataModel.mixin(PhysicalItemTemplate) {
    static defineSchema() {
        return this.mergeSchema(super.defineSchema(), {
            category: new foundry.data.fields.StringField({initial: 'melee', choices: CONFIG.R3A.weapon_types}),
            damage: new foundry.data.fields.StringField(),
            ammunition: new foundry.data.fields.NumberField({min: 0}),
            range: new foundry.data.fields.StringField(),
            dpa: new foundry.data.fields.NumberField({min: 1, integer: true, initial: 1}),
            burst: new foundry.data.fields.SchemaField({
                targets: new foundry.data.fields.NumberField({min: 1, initial: 1, integer: true}),
                damage: new foundry.data.fields.StringField()
            }, {required: false})
        })
    }
}
