/**
 * @typedef {DataFieldOptions} MappingFieldOptions
 * @property {string[]} [initialKeys]  Keys that will be created if no data is provided.
 */

/**
 * A subclass of ObjectField that represents a mapping of keys to the provided DataField type.
 *
 * @param {DataField} model                    The class of DataField which should be embedded in this field.
 * @param {MappingFieldOptions} [options={}]   Options which configure the behavior of the field.
 * @property {string[]} [initialKeys]          Keys that will be created if no data is provided.
 */
export class MappingField extends foundry.data.fields.ObjectField {
    constructor(model, options) {
        if (!(model instanceof foundry.data.fields.DataField)) {
            throw new Error("MappingField must have a DataField as its contained element");
        }
        super(options);

        /**
         * The embedded DataField definition which is contained in this field.
         * @type {DataField}
         */
        this.model = model;
    }

    /* -------------------------------------------- */

    /** @inheritdoc */
    static get _defaults() {
        return foundry.utils.mergeObject(super._defaults, {
            initialKeys: null,
            initialValue: null
        });
    }

    /* -------------------------------------------- */

    /** @inheritdoc */
    _cleanType(value, options) {
        Object.entries(value).forEach(([k, v]) => value[k] = this.model.clean(v, options));
        return value;
    }

    /* -------------------------------------------- */

    /** @inheritdoc */
    getInitialValue(data) {
        let keys = this.initialKeys;
        const initial = super.getInitialValue(data);
        if (!keys || !foundry.utils.isEmpty(initial)) return initial;
        if (!(keys instanceof Array)) keys = Object.keys(keys);
        for (const key of keys) {
            const modelInitial = this.model.getInitialValue();
            initial[key] = this.initialValue?.(key, modelInitial) ?? modelInitial;
        }
        return initial;
    }

    /* -------------------------------------------- */

    /** @override */
    _validateType(value, options = {}) {
        if (foundry.utils.getType(value) !== "Object") throw new Error("must be an Object");
        const errors = this._validateValues(value, options);
        if (!foundry.utils.isEmpty(errors)) throw new foundry.data.fields.ModelValidationError(errors);
    }

    /* -------------------------------------------- */

    /**
     * Validate each value of the object.
     * @param {object} value     The object to validate.
     * @param {object} options   Validation options.
     * @returns {Object<Error>}  An object of value-specific errors by key.
     */
    _validateValues(value, options) {
        const errors = {};
        for (const [k, v] of Object.entries(value)) {
            const error = this.model.validate(v, options);
            if (error) errors[k] = error;
        }
        return errors;
    }

    /* -------------------------------------------- */

    /** @override */
    initialize(value, model, options = {}) {
        if (!value) return value;
        return Object.entries(value).reduce((obj, [k, v]) => {
            obj[k] = this.model.initialize(v, model, options);
            return obj;
        }, {});
    }
}
