export default class RagnarokActorSheet extends ActorSheet {

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      width: 720,
      classes: ['r3a', 'sheet', 'actor'],
      tabs: [{navSelector: ".tabs", contentSelector: ".sheet-body", initial: "skills"}],
    })
  }


  activateListeners(html) {
    super.activateListeners(html);
    html.find(".item-edit").click(this._onItemEdit.bind(this))
    html.find(".item-create").click(this._onItemCreate.bind(this))
    html.find(".item-delete").click(this._onItemDelete.bind(this))
    html.find(".inventory-roll").click(this._onInventoryRoll.bind(this))
    html.find(".character_attribute .attribute_header .rollable").click(this._onRollAttributeClick.bind(this))
    html.find(".skill:not(.disabled) .skill_name .rollable").click(this._onRollSkillCheck.bind(this))
    html.find(".skill:not(.disabled) .skill_specialization .specialization_name").click(this._onRollSpecializationCheck.bind(this))
    html.find(".skill button.specialization").click(this._onClickAddSpecialization.bind(this))
    html.find(".skill button.remove_specialization").click(this._onClickRemoveSpecialization.bind(this))
    html.find("button.add-language").click(this._onAddLanguageClick.bind(this))
    html.find(".item-toggle").click(this._onToggleItem.bind(this));
  }

  async _onItemCreate(event) {
    event.preventDefault()
    const header = event.currentTarget
    const type = header.dataset.type

    const itemData = {
      name: game.i18n.format("R3A.EQUIPO.NUEVO_EQUIPO", {
        type: game.i18n.localize(`ITEM.Type${type.capitalize()}`)
      }),
      type,
      system: foundry.utils.expandObject({...header.dataset})
    }
    delete itemData.system.type
    const item = await this.actor.createEmbeddedDocuments("Item", [itemData])
    item[0].sheet.render(true)
  }

  /**
   * Handle editing an existing Owned Item for the Actor.
   * @param {Event} event    The originating click event.
   * @returns {ItemSheet5e}  The rendered item sheet.
   * @private
   */
  _onItemEdit(event) {
    event.preventDefault();
    const li = event.currentTarget.closest("[data-item-id]");
    const item = this.actor.items.get(li.dataset.itemId);
    return item.sheet.render(true);
  }

  _onItemDelete(event) {
    event.preventDefault()
    const li = event.currentTarget.closest("[data-item-id]");
    const item = this.actor.items.get(li.dataset.itemId);
    if (!item) return;
    return item.deleteDialog()
  }

  async getData(options) {
    const context = {
      ...super.getData(options),
      isGM: game.user.isGM,
      owner: this.actor.isOwner,
      editable: this.isEditable,
      system: this.actor.system,
      items: Array.from(this.actor.items),
      config: CONFIG.R3A,
      itemContext: {},
      attributes: foundry.utils.deepClone(this.actor.system.attributes),
      skills: foundry.utils.deepClone(this.actor.system.skills)
    }

    context.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    for (const [a, attr] of Object.entries(context.attributes)) {
      attr.label = game.i18n.localize(CONFIG.R3A.attributes[a])
    }
    for (const [skill_id, skill] of Object.entries(context.skills)) {
      skill.classes = []
      if (!skill.value) {
        if (!skill.can_roll_on_null) {
          skill.classes.push('disabled')
        }
        if (skill.disadvantage_on_null) {
          skill.classes.push('disadvantage')
        }
      }
      if (skill.value >= 5) {
        skill.classes.push('specialization')
      }
      skill.label = game.i18n.localize(skill.label)
    }

    context.skills = Object.keys(context.skills)
      .sort((a, b) => {
        if (context.skills[a].label < context.skills[b].label) {
          return -1
        }
        if (context.skills[a].label > context.skills[b].label) {
          return 1
        }
        return 0
      }).reduce((acc, key) => {
        acc[key] = context.skills[key];
        return acc;
      }, {})

    this._prepareItems(context)

    return context
  }

  _prepareItems(context) {
    context.advantages = context.items.filter(item => item.type === 'advantage')
    context.disadvantages = context.items.filter(item => item.type === 'disadvantage')

    let {
      protections,
      melee,
      handguns,
      shotguns,
      submachineguns,
      rifles,
      distance,
      spells,
      psychics
    } = context.items.reduce((acc, item) => {
      const ctx = context.itemContext[item.id] ??= {}
      ctx.isRollable = ['weapon', 'spell', 'psychic'].includes(item.type)
      ctx.hasBurst = CONFIG.R3A.burst_weapons.includes(item.system.category)
      this._prepareItemToggleState(item, ctx)

      if (item.type === 'protection') acc.protections.push(item)
      if (item.type === 'weapon' && item.system.category === 'hands') acc.melee.push(item)
      if (item.type === 'weapon' && item.system.category === 'melee') acc.melee.push(item)
      if (item.type === 'weapon' && item.system.category === 'handgun') acc.handguns.push(item)
      if (item.type === 'weapon' && item.system.category === 'shotgun') acc.shotguns.push(item)
      if (item.type === 'weapon' && item.system.category === 'submachinegun') acc.submachineguns.push(item)
      if (item.type === 'weapon' && item.system.category === 'rifle') acc.rifles.push(item)
      if (item.type === 'weapon' && item.system.category === 'distance') acc.distance.push(item)
      if (item.type === 'spell') acc.spells.push(item)
      if (item.type === 'psychic') acc.psychics.push(item)

      return acc
    }, {
      protections: [],
      melee: [],
      handguns: [],
      shotguns: [],
      submachineguns: [],
      rifles: [],
      distance: [],
      spells: [],
      psychics: []
    })


    const inventory = {
      protections: {
        label: "ITEM.TypeProtection",
        columns: [{id: 'vad', label: "R3A.EQUIPO.VAD", property: "system.vad"}],
        items: protections,
        dataset: {type: 'protection'}
      },
      melee: {
        label: "R3A.ARMAS.TIPOS.MELEE",
        columns: [{id: 'damage', label: "R3A.ARMAS.DANO", property: "system.damage"}],
        items: melee,
        dataset: {type: 'weapon', category: 'melee'}
      },
      handguns: {
        label: "R3A.ARMAS.TIPOS.ARMA_CORTA",
        columns: [{id: 'damage', label: "R3A.ARMAS.DANO", property: "system.damage"}],
        items: handguns,
        dataset: {type: 'weapon', category: 'handgun'}
      },
      shotguns: {
        label: "R3A.ARMAS.TIPOS.ESCOPETA",
        columns: [{id: 'damage', label: "R3A.ARMAS.DANO", property: "system.damage"}],
        items: shotguns,
        dataset: {type: 'weapon', category: 'shotgun'}
      },
      submachinegus: {
        label: "R3A.ARMAS.TIPOS.SUBFUSIL",
        columns: [{id: 'damage', label: "R3A.ARMAS.DANO", property: "system.damage"}],
        items: submachineguns,
        dataset: {type: 'weapon', category: 'submachinegun'}
      },
      rifles: {
        label: "R3A.ARMAS.TIPOS.FUSIL",
        columns: [{id: 'damage', label: "R3A.ARMAS.DANO", property: "system.damage"}],
        items: rifles,
        dataset: {type: 'weapon', category: 'rifle'}
      },
      distance: {
        label: "R3A.ARMAS.TIPOS.ARMAS_A_DISTANCIA",
        columns: [{id: 'damage', label: "R3A.ARMAS.DANO", property: "system.damage"}],
        items: distance,
        dataset: {type: 'weapon', category: 'distance'}
      }
    }
    context.inventory = Object.values(inventory)

    const magic = {
      spells: {
        label: "ITEM.TypeSpell",
        columns: [
          {id: 'lp', label: "R3A.MAGIA.PL", property: "system.lp"},
        ],
        items: spells,
        dataset: {type: 'spell'}
      },
      psychic: {
        label: "ITEM.TypePsychic",
        columns: [],
        items: psychics,
        dataset: {type: 'psychic'}
      },
    }
    context.magic = Object.values(magic)
  }

  _prepareItemToggleState(item, context) {
    const isActive = Boolean(item.system.equipped)
    context.toggleClass = isActive ? "active" : ""
    context.toggleTitle = game.i18n.localize(isActive ? "R3A.EQUIPO.EQUIPADO" : "R3A.EQUIPO.NO_EQUIPADO")
    context.canToggle = "equipped" in item.system;
  }

  _onInventoryRoll(event) {
    const type = event.currentTarget.dataset.type
    const itemId = event.currentTarget.closest("[data-item-id]").dataset.itemId
    const item = this.actor.items.get(itemId)
    switch (type) {
      case "spell":
        this.actor.rollSpell(item);
        break;
      case "weapon":
        if (!item.isRanged) {
          this.actor.rollMeleeAttack(item)
        } else {
          this.actor.rollRangedAttack(item)
        }
        break;
      case "psychic":
        this.actor.rollPsychic(item)
        break
    }


  }


  /**
   * Handle toggling the state of an Owned Item within the Actor.
   * @param {Event} event        The triggering click event.
   * @returns {Promise<RagnarokItem>}  Item with the updates applied.
   * @private
   */
  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest("[data-item-id]").dataset.itemId;
    const item = this.actor.items.get(itemId);
    return item.update({"system.equipped": !foundry.utils.getProperty(item, "system.equipped")});
  }

  _onRollAttributeClick(event) {
    const attribute = event.target.closest('.character_attribute').dataset.attribute
    return this.actor.rollAttribute(attribute, {event})
  }

  _onRollSkillCheck(event) {
    const skill = event.target.closest('.skill').dataset.skill
    return this.actor.rollSkill(skill, null, {event})
  }

  _onRollSpecializationCheck(event) {
    const skill = event.target.closest('.skill').dataset.skill
    const specialization = event.currentTarget.innerText
    return this.actor.rollSkill(skill, specialization, {event})
  }

  async _onClickRemoveSpecialization(event) {
    const skill = event.target.closest('.skill').dataset.skill
    const specialization = event.currentTarget.dataset.specialization
    await this.actor.removeSpecialization(skill, specialization)
  }

  async _onClickAddSpecialization(event) {
    const skill = event.target.closest('.skill').dataset.skill
    new Dialog({
      title: game.i18n.localize("R3A.ESPECIALIZACION.ANADIR_ESPECIALIZACION"),
      content: await renderTemplate('systems/ragnarok-3a/templates/sheets/dialog/add-specialization.hbs', {}),
      default: "add",
      buttons: {
        add: {
          label: game.i18n.localize("R3A.ANADIR"),
          callback: async html => {
            const form = html[0].querySelector("form")
            await this.actor.addSpecialization(skill, form.specialization.value)
          }
        }
      }

    }).render(true)

  }

  async _onAddLanguageClick(event) {
    new Dialog({
      title: game.i18n.localize("R3A.ESPECIALIZACION.ANADIR_ESPECIALIZACION"),
      content: await renderTemplate('systems/ragnarok-3a/templates/sheets/dialog/add-language.hbs', {}),
      default: "add",
      buttons: {
        add: {
          label: game.i18n.localize("R3A.ANADIR"),
          callback: async html => {
            const form = html[0].querySelector("form")
            const skillName = `${game.i18n.localize("R3A.HABILIDADES.IDIOMA_EXTRANJERO")}: ${form.language.value}`
            await this.actor.addSkill('intelligence', skillName, 'ie')
          }
        }
      }

    }).render(true)
  }
}
