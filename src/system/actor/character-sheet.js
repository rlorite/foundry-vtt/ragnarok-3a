import RagnarokActorSheet from "./actor-sheet.js";
import ExperienceWizard from "../apps/ExperienceWizard.mjs";

export class CharacterSheet extends RagnarokActorSheet {

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      template: 'systems/ragnarok-3a/templates/sheets/character-sheet.hbs',
      classes: [
        ...super.defaultOptions.classes,
        'character'
      ],
      height: 800,
    })
  }

  activateListeners(html) {
    html.find(".stress-roll").click(this._onRollStressClick.bind(this))
    html.find(".remove-stress-roll").click(this._onRollRemoveStressClick.bind(this))
    html.find(".actor-px h3").click(this._onExperienceClick.bind(this))
    super.activateListeners(html);
  }


  _onRollStressClick(event) {
    return this.actor.rollStress({event})
  }
  _onRollRemoveStressClick(event) {
    return this.actor.rollRemoveStress({event})
  }

  _onExperienceClick(event) {
    return ExperienceWizard.create(this.actor)
  }
}

