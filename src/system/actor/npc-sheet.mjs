import RagnarokActorSheet from "./actor-sheet.js";

export class NpcSheet extends RagnarokActorSheet {


    static get defaultOptions() {
        return {
            ...super.defaultOptions,
            template: 'systems/ragnarok-3a/templates/sheets/npc-sheet.hbs',
            classes: [
                ...super.defaultOptions.classes,
                'npc'
            ],
        }
    }

}
