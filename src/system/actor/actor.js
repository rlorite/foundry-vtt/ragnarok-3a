import StressRoll from "../dice/stress-roll.mjs";
import D20Roll from "../dice/d20-roll.mjs";
import CombatRoll from "../dice/combat-roll.mjs";
import {DamageRoll} from "../dice/dice.mjs";
import SpellRoll from "../dice/spell-roll.mjs";
import PsychicRoll from "../dice/psychic-roll.mjs";
import RemoveStressRoll from "../dice/remove-stress-roll.mjs";

export class RagnarokActor extends Actor {
  static chatListeners(html) {
    html.on("click", ".roll-actions button[data-action]", this._onChatCardAction.bind(this))
  }

  /**
   * Handle execution of a chat card action via a click event on one of the card buttons
   * @param {Event} event       The originating click event
   * @returns {Promise}         A promise which resolves once the handler workflow is complete
   * @private
   */
  static async _onChatCardAction(event) {
    event.preventDefault()
    event.stopPropagation()
    const button = event.currentTarget
    button.disabled = true
    const actor = await RagnarokActor.fromUuid(button.dataset.actor)
    if (!actor.canUserModify(game.user, "update")) {
      ui.notifications.warn("No controlas al personaje objetivo. No puedes realizar esta acción.")
      button.disabled = false
      return false
    }
    const action = button.dataset.action
    if (!actor) return;
    const card = button.closest(".chat-card");
    const messageId = card.closest(".message").dataset.messageId;
    const message = game.messages.get(messageId);
    let notification_text = ''
    let remove_action = false
    switch (action) {
      case "apply-stress":
        await actor.applyStress()
        remove_action = true
        notification_text = game.i18n.localize("R3A.ESTRES.ESTRES_APLICADO")
        break;
      case "remove-stress":
        const stress = Number(button.dataset.stress)
        await actor.removeStress(stress)
        notification_text = game.i18n.format("R3A.ESTRES.ESTRES_ELIMINADO", {stress})
        break;
      case "attack-dpa":
        await actor.rollDpa(button);
        break;
      case "roll-psychic-damage":
        const psychic = actor.items.get(button.dataset.itemId)
        const fumble = Boolean(Number(button.dataset.fumble))
        await actor.rollDamage(`${fumble ? 2 : 1}d6`, psychic.name, button)
        break
      case "roll-damage":
        const weapon = actor.items.get(button.dataset.itemId)
        let formula = weapon.system.damage
        const bd = actor?.system.bd
        if (!weapon.isRanged && bd !== '-') {
          formula += bd
        }
        await actor.rollDamage(formula, weapon.name, button);
        break
      case "apply-damage":
        const damage = await actor.applyDamage(button.dataset.damage)
        remove_action = true
        notification_text = game.i18n.format("R3A.COMBATE.DANO_APLICADO", {
          actor: actor.name,
          damage
        })
        break;
      case "consume-lp":
        const lp = Number(button.dataset.lp)
        await actor.consumeLightPoints(lp)
        remove_action = true
        notification_text = game.i18n.format("R3A.MAGIA.PL_DESCONTADOS", {lp})
        break;
      case "roll-spell-stress":
        const difficulty = Number(button.dataset.difficulty)
        await actor.rollStress({
          targetValue: difficulty,
          defaultAttribute: 'essence'
        })
        remove_action = true
        button.disabled = false
    }
    if (remove_action) {
      button.remove()
      const actions = card.querySelector(".roll-actions")
      if (!actions.childElementCount) {
        actions.remove()
      }
    }
    if (notification_text) {

      let notifications = card.querySelector('.roll-notifications')
      if (!notifications) {
        notifications = document.createElement('div')
        notifications.classList.add('roll-notifications')
        card.appendChild(notifications)
      }
      const notification = document.createElement('span')
      notification.classList.add('notification', 'info')
      notification.innerText = notification_text
      notifications.appendChild(notification)
    }
    await message.update({
      content: card.outerHTML
    })
  }

  async rollDamage(formula, itemName, button) {
    const options = {}
    let data = this.getRollData()

    return await DamageRoll.roll(formula, data, {
      ...options,
      target: button.dataset.targetId,
      flavor: game.i18n.format("R3A.TIRADAS.PROMPT_DAMAGE", {
        name: itemName
      })
    }, this)

  }

  async rollPsychic(psychic) {

    return PsychicRoll.roll({...this.getRollData()}, {
      targetValue: 21,
      psychic
    }, this)

  }

  async rollSpell(spell) {
    if (this.system.lp.value < spell.system.lp) {
      return ui.notifications.warn("R3A.MAGIA.NO_TIENES_PL", {localize: true})
    }
    return SpellRoll.roll({...this.getRollData()}, {
      targetValue: spell.system.difficulty,
      spell
    }, this)
  }

  async rollAttribute(attributeId, options = {}) {
    const attribute = this.system.attributes[attributeId]
    const data = {
      ...super.getRollData(),
      attr_mod: attribute.value
    }

    return await D20Roll.roll(data, {
      ...options,
      chooseModifier: false,
      flavor: game.i18n.format("R3A.TIRADAS.PROMPT_HABILIDAD", {
        skill: game.i18n.localize(CONFIG.R3A.attributes[attributeId] ?? '')
      })
    })
  }

  /**
   * Roll a Skill Check
   * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
   * @param {string} skillId          The skill id (e.g. "ins")
   * @param {string} specialization   The skill specialization if any
   * @param {object} options          Options which configure how the skill check is rolled
   * @returns {Promise<D20Roll>}      A Promise which resolves to the created Roll instance
   */
  async rollSkill(skillId, specialization, options = {}) {
    const skill = this.system.skills[skillId]
    const attribute = this.system.attributes[skill.attribute]
    const data = {...super.getRollData()}

    data.skill_mod = skill.value
    data.attr_mod = attribute.value
    options.defaultAttribute = skill.attribute
    let flavor = game.i18n.format("R3A.TIRADAS.PROMPT_HABILIDAD", {
      skill: game.i18n.localize(CONFIG.R3A.skills[skillId]?.label ?? '')
    })
    if (specialization) {
      options.specChecked = true
      flavor = game.i18n.format("R3A.TIRADAS.PROMPT_ESPECIALIZACION", {
        skill: game.i18n.localize(CONFIG.R3A.skills[skillId]?.label ?? ''), specialization
      })
    }
    this.determineSkillAdvantage(skill, options);
    await D20Roll.roll(data, {
      ...options, flavor
    })
  }

  determineSkillAdvantage(skill, options) {
    if (skill.value === 0 && skill.disadvantage_on_null) {
      options.disadvantage = true
    }
    if (skill.value === 10) {
      options.advantage = true
    }
  }

  async rollStress(options = {}) {
    const skill = this.system.skills.fri
    const attribute = this.system.attributes[skill.attribute]
    const data = {
      ...this.getRollData(),
      skill_mod: skill.value,
      attr_mod: attribute.value,
      stress_mod: Math.max(0, this.system.stress.pet - 1)
    }

    const roll = await StressRoll.roll(data, {
      ...options, chooseModifier: true
    }, this)
  }

  async rollRemoveStress(options) {
    const skill = this.system.skills.fri
    const attribute = this.system.attributes.willpower
    const data = {
      ...this.getRollData(),
      skill_mod: skill.value,
      attr_mod: attribute.value
    }

    await RemoveStressRoll.roll(data, {
      ...options,
      chooseModifier: false
    }, this)
  }

  /**
   * @param {RagnarokItem} weapon
   */
  async rollMeleeAttack(weapon) {
    switch (weapon.system.category) {
      case "hands":
        await this._rollAttack(this.system.skills.pel, weapon);
        break
      case 'melee':
        await this._rollAttack(this.system.skills.cac, weapon);
        break
    }
  }

  async rollRangedAttack(weapon) {
    switch (weapon.system.category) {
      case "distance":
        await this._rollAttack(this.system.skills.ad, weapon);
        break
      default:
        await this._rollAttack(this.system.skills.af, weapon);
    }
  }

  async rollDpa(event) {
    const options = {}
    const weapon = this.items.get(event.dataset.weaponId)
    const target = Array.from(game.user.targets).pop()
    const targetId = target?.actor.uuid ?? ''
    options.disadvantage = true
    if (targetId !== event.dataset.targetId) {
      options.incrDifficulty = Number(event.dataset.difficulty) + 3
    }
    options.dpa = Number(event.dataset.dpa) - 1

    const skill = this.system.skills.af
    await this._rollAttack(skill, weapon, options)
  }

  async _rollAttack(skill, weapon, options = {}) {
    const attribute = this.system.attributes[skill.attribute]
    this.determineSkillAdvantage(skill, options)
    if (game.user.targets.size > 1) {
      ui.notifications.warn(game.i18n.localize('R3A.COMBATE.AVISO_DEMASIADOS_OBJETIVOS'))
    }
    const data = {
      ...this.getRollData(),
      skill_mod: skill.value,
      attr_mod: attribute.value
    }
    await CombatRoll.roll(data, {
      ...options,
      chooseModifier: false,
      weapon,
      target: Array.from(game.user.targets).pop()
    }, this)
  }

  async applyStress() {
    let pet = this.system.stress.pet + 1
    const updates = {}
    if (pet > 3) {
      updates['system.stress.pep'] = this.system.stress.pep + 1
      ui.notifications.warn("R3A.ESTRES.AVISO_PEP", {localize: true})
      pet = 0
    }

    updates['system.stress.pet'] = pet
    await this.update(updates)
  }

  async removeStress(amount) {
    let pet = this.system.stress.pet
    pet = Math.max(0, pet - amount)
    await this.update({
      'system.stress.pet': pet
    })
  }

  async applyDamage(damage) {
    const realDamage = Math.max(0, damage - this.system.vad)
    const hp = this.system.hp.value - realDamage
    await this.update({
      'system.hp.value': hp
    })
    if (hp <= 0) {
      ui.notifications.warn(game.i18n.format("R3A.COMBATE.AVISO_DANO", {
        actor: this.name
      }))
    }
    return realDamage
  }

  async applyLPIncrement() {
    await this.update({
      'system.lp.increments': this.system.lp.increments + 1
    })
  }

  async consumeLightPoints(points) {
    await this.update({
      'system.lp.value': Math.max(0, this.system.lp.value - points),
    })
    if (this.system.lp.value === 0) {
      ui.notifications.warn("R3A.MAGIA.SIN_PL", {localize: true})
    }
  }

  async addSkill(attribute, skillName, prefix = 'ch') {
    const skills = this.system.skills
    let index = 1
    while (`${prefix}${index}` in skills) {
      index++
    }
    skills[`${prefix}${index}`] = {
      label: skillName, attribute, can_roll_on_null: false
    }

    await this.update({
      'system.skills': skills
    })
  }

  async addSpecialization(skillId, specialization) {
    const skill = this.system.skills[skillId]
    skill.specializations.push(specialization)
    const updates = {}
    updates[`system.skills.${skillId}`] = skill
    await this.update(updates)
  }

  async removeSpecialization(skillId, specialization) {
    const skill = this.system.skills[skillId]
    skill.specializations = skill.specializations.filter(spec => spec !== specialization)
    const updates = {}
    updates[`system.skills.${skillId}`] = skill
    await this.update(updates)
  }

  prepareDerivedData() {
    if (this.system.hp.value > this.system.hp.max) {
      this.system.hp.value = this.system.hp.max
    }

    if (this.type === 'character') {
      this.system.stress.level = CONFIG.R3A.stress_level[this.system.stress.pep]
      this.system.lp.max = this.system.attributes.willpower.value + this.system.attributes.essence.value + this.system.lp.increments
      if (this.system.lp.value === null || this.system.lp.value > this.system.lp.max) {
        this.system.lp.value = this.system.lp.max
      }
    }

    this.system.bd = CONFIG.R3A.bd[this.system.attributes.strength.value]
    this.system.da = CONFIG.R3A.da[this.system.attributes.dexterity.value]


    const vad = [];
    this.items.forEach(item => {
      if (!item.type === 'protection') return
      if (!item.system.equipped) return
      vad.push(item.system.vad)
    })

    if (vad.length > 0) {
      this.system.vad = Math.max(...vad)
    }

  }

  /**
   * @param uuid
   * @returns {Promise<RagnarokActor|null|*>}
   */
  static async fromUuid(uuid) {
    if (!uuid) return null
    const document = await fromUuid(uuid)
    if ('actor' in document) {
      return document.actor
    }
    return document
  }
}
