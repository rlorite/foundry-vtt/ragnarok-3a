export default class RagnarokItemSheet extends ItemSheet {
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            width: 560,
            height: 400,
            classes: ['r3a', 'item', 'sheet']
        })
    }
    get template() {
        return `systems/ragnarok-3a/templates/items/${this.item.type}.hbs`
    }

    getData(options) {
        return {
            ...super.getData(),
            config: CONFIG.R3A
        }
    }
}
