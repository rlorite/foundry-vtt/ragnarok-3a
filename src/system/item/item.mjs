export default class RagnarokItem extends Item {


    get isRanged() {
        return CONFIG.R3A.ranged_weapons.includes(this.system.category)
    }

    get hasBurst() {
        return CONFIG.R3A.burst_weapons.includes(this.system.category)
    }
}
