import './scss/style.scss'
// Import Modules
import {preloadHandlebarsTemplates, registerHandlebarsHelpers} from './system/templates.js'
import {RagnarokActor} from './system/actor/actor.js'
import {CharacterSheet} from './system/actor/character-sheet.js'
import R3A from './system/config.mjs'
import registerSettings from './system/settings.mjs'
import RagnarokItem from "./system/item/item.mjs";
import RagnarokItemSheet from "./system/item/item-sheet.mjs";
import ItemsModels from "./system/data/item/config.mjs"
import ActorModels from "./system/data/actor/config.mjs"
import {CombatRoll, D20Roll, DamageRoll, SpellRoll, StressRoll, RemoveStressRoll} from "./system/dice/dice.mjs";
import PsychicRoll from "./system/dice/psychic-roll.mjs";
import {NpcSheet} from "./system/actor/npc-sheet.mjs";
/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.once('init', async function () {
    CONFIG.R3A = R3A

    if (foundry.utils.isNewerVersion(game.version, "11")) {
        CONFIG.Actor.dataModels = ActorModels
        CONFIG.Item.dataModels = ItemsModels
    } else {
        CONFIG.Actor.systemDataModels = ActorModels
        CONFIG.Item.systemDataModels = ItemsModels

    }
    CONFIG.Actor.documentClass = RagnarokActor
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet('r3a', CharacterSheet, {
        types: ['character'],
        makeDefault: true,
        label: "R3A.FICHA.PJ"
    })
    Actors.registerSheet('r3a', NpcSheet, {
        types: ['npc'],
        makeDefault: false,
        label: "R3A.FICHA.PNJ"
    })
    CONFIG.Item.documentClass = RagnarokItem;

    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet('r3a', RagnarokItemSheet, {
        makeDefault: true
    })

    CONFIG.Combat.initiative.formula = '1d20 + @attributes.perception.value + @skills.alt.value'
    CONFIG.Dice.rolls.push(DamageRoll, D20Roll, StressRoll, CombatRoll, SpellRoll, PsychicRoll, RemoveStressRoll)
    await preloadHandlebarsTemplates()
    registerHandlebarsHelpers()
    registerSettings()
})

/**
 * Reset initiative on every round
 */
Hooks.on('combatRound', async (combat, data, options) => {
    if (combat.active) {
        for (const combatant of combat.combatants) {
            combatant.updateSource({initiative: null})
        }
        const combatantsIds = combat.combatants.map(c => c.id)
        await combat.rollInitiative(combatantsIds)
    }
})

Hooks.on('renderChatLog', (app, html, data) => RagnarokActor.chatListeners(html))
