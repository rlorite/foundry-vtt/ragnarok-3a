import webpack from "webpack";
import * as path from 'path';
import glob from 'glob';
import { fileURLToPath } from 'url';
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import WebpackBar from "webpackbar";

const __dirname = path.dirname(fileURLToPath(import.meta.url))
const buildMode = process.argv[3] === 'production' ? 'production' : 'development'

const templates = () => {
  return glob.sync('**/*.hbs', {cwd: path.join(__dirname, 'system/templates')})
    .map(file => `"systems/ragnarok-3a/templates/${file}"`)
    .join(', ')
}
export default (env) => {
  const isDevelopment = buildMode === 'development'

  const config = {
    entry: {
      system: './src/system.mjs'
    },
    watch: buildMode === 'development',
    devtool: 'inline-source-map',
    mode: buildMode,
    stats: 'minimal',
    resolve: {
      extensions: ['.js'],
      alias: {
        'assets': path.resolve(__dirname, 'src/assets')
      },
      fallback: {
        "fs": false,
        "path": false,
        "assert": false,
        "util": false,
      }
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/systems/ragnarok-3a/',
      assetModuleFilename: 'assets/[name].[hash:8][ext]',
    },
    devServer: {
      hot: true,
      port: 8020,
      proxy: [
        {
          context: (pathname) => {
            return !pathname.match('^/sockjs')
          },
          target: 'http://localhost:30011',
          ws: true,
        },
      ],
    },
    module: {
      rules: [
        isDevelopment
          ? {test: /\.hbs$/, loader: 'raw-loader',}
          : {test: /\.hbs$/, loader: 'null-loader',},
        {
          test: /templates\.js$/,
          use: [
            {
              loader: 'string-replace-loader',
              options: {
                search: "'__ALL_TEMPLATES__'",
                replace: templates
              }
            }
          ]
        },
        {
          test: /\.(png|jpg|jpeg|gif|ico|svg|webp)$/,
          type: 'asset/resource',
          generator: {filename: 'assets/images/[name].[hash:8][ext]'}
        },
        {
          test: /\.(woff|woff2|ttf|eot|otf)$/,
          type: 'asset/resource',
          generator: {filename: 'assets/fonts/[name].[hash:8][ext]'}
        },
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {loader: 'css-loader', options: {sourceMap: isDevelopment}},
            {
              loader: "sass-loader",
              options: {
                sourceMap: isDevelopment,
                implementation: 'sass'
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new CopyPlugin({
        patterns: [{from: 'system'}]
      }),
      new MiniCssExtractPlugin({
        filename: '[name].css'
      }),
      new webpack.ProvidePlugin({
        process: 'process/browser'
      }),
      new WebpackBar({})
    ]


  }
  if (!isDevelopment) {
    delete config.devtool
  }

  return config
}
